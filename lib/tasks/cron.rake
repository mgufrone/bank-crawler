require 'chronic'
require 'thread'
require 'childprocess'
namespace :cron do

  desc "Grab all transactions list from active registered bank accounts"
  task transactions: :environment do

    # job_id = Digest::MD5.hexdigest(Time.now.utc.to_s).to_s
    # log_job = LogId.create(name: 'engine.banks', job_id: job_id)
    if Rails.env == 'production'
      headless = Headless.new(:display => "1366x768")
      headless.start
    end

    process = ChildProcess.build("sidekiq","-C","config/sidekiq.yml")
    process.io.inherit!
    process.environment["RAILS_ENV"] = Rails.env
    process.cwd = Rails.root
    begin
      process.start
      process.wait
    rescue => e
      process.stop
    end

   if Rails.env == 'production'
     headless.destroy
   end

  end

end

require 'thread'
require_relative '../../app/helpers/bni_helper'
require_relative '../../app/helpers/bca_helper'
require_relative '../../app/helpers/mandiri_helper'
require_relative '../../app/helpers/mandiri_bisnis_helper'
namespace :banks do
  desc "Create new BCA bank Grabber instance"
  task :bca, [:bank_id, :start_date, :end_date] => [:environment] do |t, args|
    # if Setting.where(name: 'engine.cron').first[:value] == 'on'
      start_date = args[:start_date]
      start_date = '1 day ago' if start_date == nil
      end_date = args[:end_date]
      end_date = Time.now.strftime('%Y-%m-%d') if end_date == nil

      bank = Bank.find(args[:bank_id])
      args = {
        :bank=>bank,
        :start_date=>Chronic.parse(start_date).strftime('%Y-%m-%d'),
        :end_date=>Chronic.parse(end_date).strftime('%Y-%m-%d')
      }
      headless = Headless.new(:display => "1366x768")
      headless.start
      BankHelper::Bca.new(args)
      headless.destroy
  	# end
  end

  desc "Create new Mandiri bank Grabber instance"
  task :mandiri, [:bank_id, :start_date, :end_date] => [:environment] do |t, args|
	    # if Setting.where(name: 'engine.cron').first[:value] == 'on'

        start_date = args[:start_date]
        start_date = '1 day ago' if start_date == nil
        end_date = args[:end_date]
        end_date = Time.now.strftime('%Y-%m-%d') if end_date == nil

		  	bank = Bank.find(args[:bank_id])
		  	args = {
          :bank=>bank,
          :start_date=>Chronic.parse(start_date).strftime('%Y-%m-%d'),
          :end_date=>Chronic.parse(end_date).strftime('%Y-%m-%d')
        }
		  	BankHelper::Mandiri.new(args)
		# end
	end
  desc "Create new Mandiri Bisnis bank Grabber instance"
  task :mandiri_bisnis, [:bank_id, :start_date, :end_date] => [:environment] do |t, args|
	  # if Setting.where(name: 'engine.cron').first[:value] == 'on'
       start_date = args[:start_date]
       start_date = '1 day ago' if start_date == nil
       end_date = args[:end_date]
       end_date = Time.now.strftime('%Y-%m-%d') if end_date == nil

       bank = Bank.find(args[:bank_id])
       args = {
         :bank=>bank,
         :start_date=>Chronic.parse(start_date).strftime('%Y-%m-%d'),
         :end_date=>Chronic.parse(end_date).strftime('%Y-%m-%d')
       }
       BankHelper::MandiriBisnis.new(args)
		# end
  end
  desc "Create new Bank BNI instance"
  task :bni, [:bank_id, :start_date, :end_date] => [:environment] do |t, args|
    # if Setting.where(name: 'engine.cron').first[:value] == 'on'
	  	bank = Bank.find(args[:bank_id])
      start_date = args[:start_date]
      start_date = '1 day ago' if start_date == nil
      end_date = args[:end_date]
      end_date = Time.now.strftime('%Y-%m-%d') if end_date == nil
	  	args = {
        :bank=>bank,
        :start_date=>Chronic.parse(start_date).strftime('%Y-%m-%d'),
        :end_date=>Chronic.parse(end_date).strftime('%Y-%m-%d')
      }
      BankHelper::Bni.new(args)
	  # end
	end
end

# require "rvm/capistrano"
# require "bun  dler/capistrano"
# require 'capistrano/rvm'

set :application, "bank-crawler"  # EDIT your app name
set :repo_url,  "git@bitbucket.org:mgufrone/bank-crawler.git" # EDIT your git repository
set :deploy_to, "/var/www/bank-crawler" # EDIT folder where files should be deployed to
set :linked_dirs, %w{log vendor/bundle}
set :branch, "master"
set :default_env, { rvm_bin_path: '/usr/local/rvm/bin', rails_env: :production, path: "$PATH:/usr/local/rvm/bin" }
set :bundle_flags, '--deployment'
namespace :deploy do
    after :updated, "deploy:cleanup_assets"
    after :updated, "deploy:compile_assets"
    after :updated, "deploy:migrate"
    after :updated, :build do
        on roles(:app) do
            within release_path  do
              with fetch(:default_env) do
                execute :bundle, "exec rake assets:clean"
                execute :bundle, "exec rake assets:precompile"
                execute :bundle, "exec rake db:migrate"
                execute :service, "supervisor stop"
                text = [
                  "[program:bank-crawler]",
                  "command       = /usr/local/rvm/gems/ruby-2.1.5/wrappers/bundle exec /usr/local/rvm/gems/ruby-2.1.5/wrappers/rake cron:transactions",
                  "process_name 	= %(program_name)s",
                  "autostart     = true",
                  "autorestart	  = true",
                  "directory     = #{release_path}",
                  'environment 	= RAILS_ENV="production",REDIS_URL="redis://101.50.3.28:6379/0"',
                  'startretries 	= 9999',
                ].join("\n")
                # text = File.read("#{release_path}/config/deploy/template.txt").gsub('{release_path}', release_path)
                execute :echo, "'#{text}' > /etc/supervisor/conf.d/crawler.conf"
                # execute :supervisorctl, "reread"
                # execute :supervisorctl, "update"
                execute :touch, "/etc/supervisor/conf.d/crawler.conf"
                execute :service, "supervisor start"
                execute :service, "apache2 restart"
                # execute :nohup, "bundle exec rake cron:transactions RAILS_ENV=production > log/transactions.out 2>&1&"
                execute :touch, "tmp/callbacks.json"
                execute :mkdir, "-p tmp/pids"
                execute :echo, "'{}' > tmp/callbacks.json"
                execute :chown, "www-data:www-data -R ."
                execute :chmod, "0755 -R tmp"
              end
            end
        end
    end

    desc "Restart"
    after :finished, :build do
        on roles(:app) do
            within release_path  do
                # execute :rm, "Capfile"
                # execute :rm, "-rf config/deploy"
                # execute :rm, "config/deploy.rb"
            end
        end
    end

end

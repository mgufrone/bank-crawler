require 'sidekiq/web'
require 'sidekiq-cron'
Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  [user, password] == ["api", "28ER67fi"]
end
Payments::Application.routes.draw do
  get 'notification/query'
  get 'notification/:notification/splashed' => 'notification#splashed', as: :splash_notification

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :banks
  # post 'admin/bank/:id/queue' => 'admin/bank#queue', as: :queue_admin_bank
  get 'screenshot/show'

  get 'screenshots/:dir/:file', to: "screenshot#show",  :as => :screenshot, :defaults => {format: 'png'}

  get "home/index"
  get "api/queued_jobs"
  get "api/register_job"
  get "api/process_jobs"
  get "api/retry_jobs"
  get "api/clear-crons" => "api#clear_crons"
  post "api/register-job" => "api#register_job"
  post "api/remove-job" => "api#remove_job"
  mount Sidekiq::Web => '/sidekiq'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

require_relative '../../app/helpers/bca_helper'
class BcaWorker
  include Sidekiq::Worker
  sidekiq_options :queue => "bca", :retry => 5
  def perform (id, start_date=nil, end_date=nil)
    # start_date = args[:start_date]
    start_date = '1 day ago' if start_date == nil
    # end_date = args[:end_date]
    end_date = Time.now.strftime('%Y-%m-%d') if end_date == nil

    bank = Bank.find(id)
    args = {
      :bank=>bank,
      :start_date=>Chronic.parse(start_date).strftime('%Y-%m-%d'),
      :end_date=>Chronic.parse(end_date).strftime('%Y-%m-%d')
    }
    BankHelper::Bca.new(args)

      # Kernel.const.
    #   process = ChildProcess.build("rake","banks:#{bank_type}[#{bank.id}]")
    #   process.io.inherit!
    #   process.environment["RAILS_ENV"] = Rails.env
    #   process.cwd = Rails.root
    # begin
    #   process.start
    #   process.wait
    # rescue => e
    #   process.stop
    # end
  end
end

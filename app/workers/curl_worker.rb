require 'net/http'
class CurlWorker
  include Sidekiq::Worker
  sidekiq_options :retry => 5
  def perform (data)
    uri = URI(data["url"])
    req = Net::HTTP::Post.new(uri,{'Content-Type' =>'application/json'})
    if data.include?("auth")
      req.basic_auth data["auth"]["0"], data["auth"]["1"] if data["auth"].include?("0")
      req.basic_auth data["auth"][0], data["auth"][1] unless data["auth"].include?("0")
    end
    req.body = data["data"].to_json
    req.add_field("Accept", "application/json")
    req.add_field("Content-type", "application/json")
    response = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(req)
    end
  end
end

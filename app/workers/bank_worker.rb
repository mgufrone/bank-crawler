class BankWorker
  include Sidekiq::Worker
  sidekiq_options :retry => 5
  def perform (id)

      bank = Bank.find(id)
      bank_type = Bank.bank_codes.invert[bank.bank_type]
      "#{bank_type}_worker".camelize.constantize.perform_async(bank.to_param)
      # Kernel.const.
    #   process = ChildProcess.build("rake","banks:#{bank_type}[#{bank.id}]")
    #   process.io.inherit!
    #   process.environment["RAILS_ENV"] = Rails.env
    #   process.cwd = Rails.root
    # begin
    #   process.start
    #   process.wait
    # rescue => e
    #   process.stop
    # end
  end
end

class NotificationController < ApplicationController
  include NotificationHelper
  def query
    render :json => [] if current_admin_user == nil
    render :json => notifications
  end
  def splashed
    @notification = Notification.find(params[:notification])
    @notification.splashed = true
    @notification.save
    render :json => @notification
  end
end

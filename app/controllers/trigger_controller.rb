class TriggerController < ApplicationController
  def index
    i = 0
    Bank.where(allow_crawl: 'yes').where("bank_type IN (?)", ["bca", "mandiri", "mandiri-bisnis"]).each do |bank|
      i += 1
      BankWorker.perform_async(bank.id)
    end
    # RcWorker.delay_for(5.seconds).perform_async
    # SrsxWorker.delay_for(15.seconds).perform_async

    render :json => {"message"=>"Job Triggered", "result"=>"success"}
  end
  def mandiri_bisnis
    i = 0
    Bank.where(allow_crawl: 'yes').where("bank_type IN (?)", ["mandiri-bisnis"]).each do |bank|
      i += 1
      BankWorker.perform_async(bank.id)
    end

    render :json => {"message"=>"Job Triggered", "result"=>"success"}
  end
  def reseller
    RcWorker.perform_async
    SrsxWorker.perform_async

    render :json => {"message"=>"Job Triggered", "result"=>"success"}
  end
end

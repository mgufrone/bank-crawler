class ApiController < ApplicationController
  http_basic_authenticate_with name: "api", password: "28ER67fi"
  skip_before_filter :verify_authenticity_token
  def jobs

  end

  def queued_jobs
    render :json => {"jobs"=>Sidekiq::Queue.new}, :callback => params[:callback]
  end
  def process_jobs
    render :json => {"jobs"=>Sidekiq::ScheduledSet.new}, :callback => params[:callback]
  end
  def retry_jobs
    render :json => {"jobs"=>Sidekiq::RetrySet.new}, :callback => params[:callback]
  end
  def convert (data)
    result = []
    if data.kind_of? Hash
      data.each { |key, value| result.push(value)}
    else
      result = data
    end
    result
  end
  def register_job
    worker = params[:worker]
    argument = convert(params[:arguments])
    callbacks = ''
    callbacks = params[:callbacks] if params.include?(:callbacks)
    notation = params[:notation] if params.include?(:notation)
    notation = 'once' unless params.include?(:notation)

    response = {}
    if worker == 'BankWorker'
      bank = Bank.find(argument[0])
      if callbacks != ''
        file = File.open("#{Rails.root.join('tmp')}/callbacks.json", "a+")
        content = JSON.parse(File.read(file))
        key = "bank-#{bank.name.gsub(' ', '-').downcase}"
        content = {key=>[]} unless content.include?(key)
        callbacks["arguments"] = convert(callbacks["arguments"])
        content[key] << callbacks
        file.truncate(file.pos)
        file.write(content.to_json)
        file.close
      end
      file = File.open("#{Rails.root.join('tmp')}/callbacks.json", "a+")
      current_callbacks = File.read(file.path)
      response = register_worker(bank.name, worker, argument, notation)
    else
      # klass =
      response = {:message => "Job already scheduled"}
    end
    render :json => response
  end
  def clear_crons
    Sidekiq::Cron::Job.destroy_all!

    render :json => {"message" => "Cron job cleared", "result" => "success"}
  end
  def remove_job
    worker = params[:worker]
    argument = convert(params[:arguments])
    callbacks = ''
    callbacks = params[:callbacks] if params.include?(:callbacks)
    notation = params[:notation] if params.include?(:notation)
    notation = 'once' unless params.include?(:notation)

    response = {}
    if worker == 'BankWorker'
      bank = Bank.find(argument[0])
      Sidekiq::Queue.new.each do |job|
        job.delete if job.klass = "BankWorker" and job.args = argument
      end
      Sidekiq::RetrySet.new.each do |job|
        job.delete if job.klass = "BankWorker" and job.args = argument
      end
      Sidekiq::ScheduledSet.new.each do |job|
        job.delete if job.klass = "BankWorker" and job.args = argument
      end
      Sidekiq::Cron::Job.find("bank-#{bank.name.gsub(' ', '-').downcase}").destroy if Sidekiq::Cron::Job.find("bank-#{bank.name.gsub(' ', '-').downcase}") != nil
      render :json => {"message" => "Job cleared", "result" => "success"}
    elsif ['RcWorker', 'SrsxWorker', 'GogetsslWorker'].include?(worker)
      Sidekiq::Queue.new.each do |job|
        job.delete if job.klass = worker
      end
      Sidekiq::RetrySet.new.each do |job|
        job.delete if job.klass = worker
      end
      Sidekiq::ScheduledSet.new.each do |job|
        job.delete if job.klass = worker
      end
      Sidekiq::Cron::Job.find("bank-#{worker.gsub(' ', '-').downcase}").destroy if Sidekiq::Cron::Job.find("bank-#{worker.gsub(' ', '-').downcase}") != nil
      render :json => {"message" => "Job cleared", "result" => "success"}
    end
  end
end

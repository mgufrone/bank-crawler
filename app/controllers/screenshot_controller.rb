class ScreenshotController < ApplicationController
  def show
    file_name = "#{Rails.root.join("log/screenshots")}/#{params[:dir]}/#{params[:file]}.png"
    send_data open(file_name,"rb"){|f|f.read}, type: 'image/png', disposition: 'inline'
  end
end

# BEON Bank Crawler

## Important Notes

This package is working only at *nix-based OS. You can't either develop or test this package on Windows. 

## Requirements 

- ImageMagick (libmagickwand-dev, imagemagick)
- Xvfb (xvfb)
- Firefox Browser
- Ruby on Rails 4.1.1
- Ruby 2.1.2
- Mysql Client Library
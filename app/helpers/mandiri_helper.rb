require 'RMagick'
include Magick
require_relative 'bank_helper'
module MandiriHelper
end
module BankHelper
  class Mandiri < BankHelper::Bank
    def initialize (args)
      @account_username = args[:bank][:username]
      @account_password = args[:bank][:password]
      @account_id = args[:bank][:account_id]
      @bank = args[:bank]
      @start_date = Chronic.parse args[:start_date]
      @end_date = Chronic.parse args[:end_date]
      @engine = "engine.banks.mandiri.#{@account_id}"
      @job_id = build_job(@engine)
      next_process = false
      if caught_signal(@bank)
        log "Bank is still inactive"
        return
      end
      begin

        open_banking
        login
        query_saldo
        navigate_mutation
        contents = query_mutation
        logout
        next_process = true
      rescue Selenium::WebDriver::Error::UnhandledAlertError => e
        catch_error(e, 'alert')
      rescue => e
        logout
        # puts e.message
        # puts e.backtrace
        catch_error(e, 'message')
      ensure
        kill_hanging_process
      end

      if next_process
        log "Close Browser"
        close_browser
        process_content(contents)
      end
    end
    def open_banking
      @browser = prepare_browser 'https://ib.bankmandiri.co.id/retail/Login.do?action=form&lang=in_ID'
      log "Open Mandiri Internet Banking"
    end
    def query_saldo
      @browser.frame(:name => "toc").wait_until_present
      # @browser.frame(:name => "toc").link(:text=>"Beli").click
      begin
        @browser.frame(:name => "toc").link(:text=>"Informasi Rekening").click
        @browser.frame(:name => "toc").link(:text=>/tabungan/i).wait_until_present
        log "Looking for balance info"
        @browser.frame(:name => "toc").link(:text=>/tabungan/i).click
      rescue => e
        log "Looks like it is using english as the language"
        @browser.frame(:name => "toc").link(:text=>/summary/i).click
        @browser.frame(:name => "toc").link(:text=>/summary/i).wait_until_present
        log "Looking for balance info"
        @browser.frame(:name => "toc").link(:text=>/transaction\shistory/i).click
      end
      @browser.frame(:name => "CONTENT").span(:text => /daftar\srekening/i).wait_until_present
      td = @browser.frame(:name => /content/i).table(:text => /#{@account_id}/).table(:text => /#{@account_id}/).tr(:text => /#{@account_id}/)
      # puts td
      td.select_list(:name => /saselect/i).select /informasi\ssaldo/i
      @browser.frame(:name => "CONTENT").wait_until_present
      content = Nokogiri::HTML.parse(@browser.frame(:name => "CONTENT").html)
      saldo = URI.decode(URI.encode(content.css("td:contains(\"Informasi Saldo\") + td + td").text).gsub("%C2%A0", '')).gsub('Rp.', '').gsub(/\s|\t|\r|\n/, '').gsub(".","").gsub(",", ".").to_f
      @bank.balances.create(amount: saldo, currency: 'IDR')
      log "Current balance : #{saldo}"
    end
    def login
      @browser.text_field(:name => 'userID').set @account_username
      @browser.text_field(:name => 'password').set @account_password
      log "Logging In"
      @browser.button(:name => 'image').click

      if @browser.font(:class => 'alert').exists?
        raise @browser.font(:class => 'alert').text
      end
    end
    def navigate_mutation
      @browser.frame(:name => "toc").wait_until_present
      # @browser.frame(:name => "toc").link(:text=>"Beli").click
      begin
        # @browser.frame(:name => "toc").link(:text=>"Informasi Rekening").click
        @browser.frame(:name => "toc").link(:text=>"Mutasi Rekening").wait_until_present
        log "Looking for mutation menu"
        @browser.frame(:name => "toc").link(:text=>"Mutasi Rekening").click
      rescue => e
        log "Looks like it is using english as the language"
        # @browser.frame(:name => "toc").link(:text=>/summary/i).click
        @browser.frame(:name => "toc").link(:text=>/summary/i).wait_until_present
        log "Looking for mutation menu"
        @browser.frame(:name => "toc").link(:text=>/transaction\shistory/i).click
      end
    end
    def query_mutation
      @browser.frame(:name => "CONTENT").select_list(:name=>"fromAccountID").select(Regexp.new("#{@account_id.to_s}"))
      @browser.frame(:name => "CONTENT").select_list(:name=>"fromDay").option(:value=>@start_date.day.to_s).select
      @browser.frame(:name => "CONTENT").select_list(:name=>"fromMonth").option(:value=>@start_date.month.to_s).select
      @browser.frame(:name => "CONTENT").select_list(:name=>"fromYear").option(:value=>@start_date.year.to_s).select

      @browser.frame(:name => "CONTENT").select_list(:name=>"toDay").option(:value=>@end_date.day.to_s).select
      @browser.frame(:name => "CONTENT").select_list(:name=>"toMonth").option(:value=>@end_date.month.to_s).select
      @browser.frame(:name => "CONTENT").select_list(:name=>"toYear").option(:value=>@end_date.year.to_s).select
      log "Get mutation data since #{@start_date.strftime('%Y-%m-%d')} to #{@end_date.strftime('%Y-%m-%d')}"
      @browser.frame(:name => "CONTENT").execute_script("formSubmit();")
      # puts @browser.frame(:name => "CONTENT").link(:id=>/linksubmit/i).html
      # @browser.frame(:name => "CONTENT").link(:id=>/linksubmit/i).click
      log "Data received. Save data"
      content = @browser.frame(:name => "CONTENT").html
    end
    def logout
      log "Logging out bank"
      @browser.frame(:name => "top").link(:text=>"LOGOUT").click if @browser != nil and @browser.frame(:name => "top").link(:text=>"LOGOUT").exists?
    end
    def process_content(content)
      doc = Nokogiri::HTML.parse(content)
      doc.css("br").each { |node| node.replace("\n")  }
      mutations = []

      contents = doc.css('form[name="TrxHistoryInqForm"] table table tbody tr')
      contents.each do |element|
        data = element.css('td.tabledata')
        if(data.any? and data.length>3)
          debit = data[2].text.gsub('.', '|')
          .gsub(',', '.')
          .gsub('|', '')

          credit = data[3].text.gsub('.', '|')
          .gsub(',', '.')
          .gsub('|', '')
          date =  data[0].text.gsub(/^(\d{2})\/(\d{2})\/(\d{4})$/, '\3-\2-\1')
          results = {
            "date"=> Chronic.parse(date).strftime('%Y-%m-%d'),
            "notes"=>data[1].text,
            "amount"=>credit.to_f>0?(credit.to_f):(-debit.to_f),
          }
          results["serial_key"] = create_key(
            "#{results["amount"]}#{results["notes"]}#{results["date"]}"
          )
          mutations << results
        end
      end
      process_data(mutations, @bank, @start_date.strftime("%Y-%m-%d"))
      @job_id.update(status: 'completed')
    end
  end
end

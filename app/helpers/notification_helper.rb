module NotificationHelper
  def notifications
    Notification.joins(:user).where('user_id = ?', current_admin_user.id).order(splashed: :asc).order(created_at: :desc).take 5
  end
  def notify(title, message, user)
    Notification.create(title: title, message: message, user_id: user)
  end
end

module LoggerHelper
  def build_job (engine_name)
      job_id = Digest::MD5.hexdigest(Time.now.utc.to_s).to_s
      Log.create(name: "#{engine_name}", job_id: job_id, status: 'running')
  end
end

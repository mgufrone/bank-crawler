require 'RMagick'
include Magick
require_relative 'bank_helper'
module BniHelper
end
module BankHelper
  class Bni < BankHelper::Bank
    def initialize (args)
      # headless = Headless.new
      # headless.start
      @account_id = args[:bank][:account_id]
      @engine = "engine.banks.bni.#{@account_id}"
      @bank = args[:bank]
      @job_id = build_job(@engine)

      @start_date = Chronic.parse args[:start_date]
      @end_date = Chronic.parse args[:end_date]
      continue_process = true
      if caught_signal(@bank)
        log "Bank is still inactive"
        return
      end
      contents = []
      begin
        @browser = prepare_browser 'https://ibank.bni.co.id/corp/AuthenticationController?__START_TRAN_FLAG__=Y&FORMSGROUP_ID__=AuthenticationFG&__EVENT_ID__=LOAD&FG_BUTTONS__=LOAD/ACTION.LOAD=Y&AuthenticationFG.LOGIN_FLAG=1&BANK_ID=BNI01&LANGUAGE_ID=002'
        log "Opening BNI Internet Banking"
        captcha_text = read_captcha
        login (captcha_text)
        query_saldo
        navigate_mutation
        contents = query_mutation
        # total_pages

      rescue => e
        logout
        catch_error(e, 'message')
        continue_process = false
      ensure
        kill_hanging_process
      end
      begin
        if continue_process
          logout
          # puts content
          process_content(contents)
          clean_up
        end
      rescue => e
        catch_error(e, 'message')
      end
      # b.destroy
      # headless.destroy
    end
    def login (captcha_text)
      @browser.text_field(:id => 'AuthenticationFG.VERIFICATION_CODE').set captcha_text
      @browser.text_field(:name => 'AuthenticationFG.USER_PRINCIPAL').set @bank[:username]
      @browser.text_field(:name => 'AuthenticationFG.ACCESS_CODE').set @bank[:password]
      log "Loggin In"
      @browser.input(:id => 'VALIDATE_CREDENTIALS').click
      if @browser.div(:class =>"redbg").exists?
        raise @browser.div(:class =>"redbg").text
      end
    end
    def query_saldo
      log "Looking for mutations menu"
      @browser.link(:id => "REKENING").wait_until_present
      @browser.link(:id => "REKENING").click
      @browser.link(:text => /saldo\srekening/i).wait_until_present
      @browser.link(:text => /saldo\srekening/i).click
      @browser.h2(:text => /daftar\ssaldo/i).wait_until_present
      content = Nokogiri::HTML.parse(@browser.html)
      saldo = content.css("h2:contains(\"Daftar Saldo Rekening\") + div table tr td:contains(\"#{@account_id}\") + td + td + td + td + td").text.gsub('IDR', '').gsub(/\s|\t|\n/, '').gsub(",", "").to_f
      @bank.balances.create(amount: saldo, currency: 'IDR')
      log "Current balance : #{saldo}"
    end
    def navigate_mutation
      log "Looking for mutations menu"
      @browser.link(:id => "REKENING").wait_until_present
      @browser.link(:id => "REKENING").click
      @browser.link(:text => /mutasi tabungan \& giro/i).wait_until_present
      @browser.link(:text => /mutasi tabungan \& giro/i).click
    end
    def query_mutation
      contents = []
      @browser.text_field(:id => "AccountSummaryFG.ACCOUNT_NUMBER").wait_until_present
      @browser.text_field(:id => "AccountSummaryFG.ACCOUNT_NUMBER").set @bank[:account_id]
      log "get mutations from #{@start_date.strftime('%d %b %Y')} to #{@end_date.strftime('%d %b %Y')}"
      @browser.button(:id => "LOAD_ACCOUNTS").click

      @browser.button(:id => "VIEW_TRANSACTION_HISTORY").wait_until_present
      @browser.button(:id => "VIEW_TRANSACTION_HISTORY").click
      @browser.text_field(:id => "TransactionHistoryFG.FROM_TXN_DATE").wait_until_present
      @browser.text_field(:id => "TransactionHistoryFG.FROM_TXN_DATE").set @start_date.strftime('%d-%b-%Y')
      @browser.text_field(:id => "TransactionHistoryFG.TO_TXN_DATE").set @end_date.strftime('%d-%b-%Y')
      @browser.button(:id => "SEARCH").click
      begin
        @browser.table(:id => "txnHistoryList").wait_until_present
        contents << @browser.table(:id => "txnHistoryList").html
        total_pages = @browser.span(:class => /paginationtxt/).text.gsub(/halaman 1 dari/i, "") if @browser.span(:class => /paginationtxt/).exists?
        total_pages = 1 unless @browser.span(:class => /paginationtxt/).exists?
        log "Total pages found : #{total_pages}"
        current_page = 2
        while current_page <= total_pages.to_i do
          @browser.text_field(:id => "TransactionHistoryFG.OpTransactionListing_REQUESTED_PAGE_NUMBER").set current_page.to_s
          @browser.button(:id => "Action.OpTransactionListing.GOTO_PAGE__").click
          @browser.table(:id => "txnHistoryList").wait_until_present
          contents << @browser.table(:id => "txnHistoryList").html
          log "Move to page #{current_page}"
          current_page += 1
        end
      rescue => e
        if @browser.div(:text => /tidak\sditemukan\stransaksi/i).exists?
          raise Nokogiri::HTML.parse(@browser.html).css('#MessageDisplay_TABLE .redbg').text
        end
      end
      contents
    end
    def logout
      if @browser.exists? and @browser.link(:id => "HREF_Logout").exists?
        log "Logging Out"
        @browser.link(:id => "HREF_Logout").click
        @browser.button(:id => "LOG_OUT").wait_until_present
        @browser.button(:id => "LOG_OUT").click
      end
    end
    def process_content(contents)
      log "Processing Data"
      mutations = []
      contents.each do |content|
        docs = Nokogiri::HTML.parse(content)
        docs.css('tbody tr:gt(2)').each do |tr|
          next if tr.css('td:eq(1) span').text.gsub(/\s+/, "") == ''
          amount = tr.css('td:eq(4) span').text.gsub(",", "").to_f
          saldo = tr.css('td:eq(5) span').text.gsub(",", "").to_f
          results = {
            "date"=> Chronic.parse(tr.css('td:eq(1) span').text).strftime('%Y-%m-%d'),
            "notes"=>tr.css('td:eq(2) span').text,
            "amount" =>tr.css('td:eq(3) span').text=='Cr.'?(amount):-(amount),
          }
          results["serial_key"] = create_key(
            "#{saldo}#{results["amount"]}#{results["date"]}"
          )
          mutations << results
        end
      end
      process_data(mutations, @bank, @start_date)
      @job_id.update(status: 'completed')
    end
    def clean_up
      @browser.close
      log "Cleanup Temporary Files"
      File.delete(Rails.root.join("tmp/captcha.png")) if File.exist?(Rails.root.join("tmp/captcha.png"))
      File.delete(Rails.root.join("tmp/screenshot.png")) if File.exist?(Rails.root.join("tmp/screenshot.png"))
    end
    def read_captcha
      log "Read Captcha"
      @browser.screenshot.save(Rails.root.join("tmp/screenshot.png"))
      @cached_browser = @browser

      i = Magick::Image.read(Rails.root.join("tmp/screenshot.png")).first
      i.crop(231,293,120,22).write(Rails.root.join("tmp/captcha.png"))
      image = Rails.root.join("tmp/captcha.png")
      text = ''
      while text == '' do
        begin
          log "Reading captcha using OCRConvert.com"
          text = ocr_convert_read_captcha(image)
        rescue => e
          @browser.close if @browser.exists?
          log e.message
          log "Fail over raised"
          log "Reading captcha using NewOCR.com"
          text = new_ocr_read_captcha(image)
        end
        if text == ''
          log "text is still empty. retrying"
        end
      end
      @browser = @cached_browser
      text
    end
    def ocr_convert_read_captcha (image)
      @browser = prepare_browser('http://www.ocrconvert.com/')
      log "Opening OCRConvert.com"
      begin
        @browser.file_field(:id => 'first-file').wait_until_present
        @browser.file_field(:id => 'first-file').set(image)
        log "Uploading captcha file"
        @browser.button(:id => "upload-button").click
        @browser.link(:text => Regexp.new("#{File.basename image, File.extname(image)}")).wait_until_present
        link = @browser.link(:text => Regexp.new("#{File.basename image, File.extname(image)}"))
        log "Getting link"
        @browser.goto link.href
        text = @browser.text.gsub(/\s+/, "")
        log "Reading Text"
      rescue => e
        @browser.close if @browser.exists?
        raise "Error occured while reading captcha via OCRConvert.com ( #{e.message} )"
      end
      @browser.close
      text
    end
    def new_ocr_read_captcha (image)
      text = ''
      @browser = prepare_browser 'http://www.newocr.com/'
      log "Opening NewOcr.com"
      begin
        @browser.file_field(:id => 'userfile').wait_until_present
        @browser.file_field(:id => 'userfile').set(image)
        log "Uploading Image"
        @browser.button(:type => "submit").click

        @browser.button(:id =>"ocr").wait_until_present
        log "Parsing Captcha"
        @browser.button(:id =>"ocr").click
        @browser.textarea(:id => "ocr-result").wait_until_present
        text = @browser.textarea(:id => "ocr-result").text
        log "Grab captcha text"
      rescue => e
        log "Error occured while reading captcha via NewOcr.com"
        @browser.close
        raise "Captcha reading error : #{e.message}"
      end
      @browser.close
      text
    end
  end
end

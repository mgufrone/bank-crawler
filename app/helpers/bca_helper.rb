require 'RMagick'
include Magick
require_relative 'bank_helper'
module BcaHelper
end
module BankHelper
  class Bca < BankHelper::Bank
    def initialize (args)
      @bank = args[:bank]
      @get_start_date = Chronic.parse args[:start_date]
      @get_end_date = Chronic.parse args[:end_date]
      @start_date = {
        "date" => @get_start_date.day<10? ("0#{@get_start_date.day}"):(@get_start_date.day.to_s),
        "month" => @get_start_date.month.to_s,
        "year" => @get_start_date.year.to_s
      }
      @end_date = {
        "date" => @get_end_date.day<10?("0#{@get_end_date.day}") :(@get_end_date.day.to_s),
        "month" => @get_end_date.month.to_s,
        "year" => @get_end_date.year.to_s
      }
      @account_username = args[:bank][:username]
      @account_password = args[:bank][:password]
      @account_id = args[:bank][:account_id]
      @engine = "engine.banks.bca.#{@account_id}"
      @job_id = build_job(@engine)
      if caught_signal(@bank)
        log "Bank is still inactive"
        return
      end
      continue_process = false
      begin
        login
        query_saldo
        navigate_mutation
        sources = query_mutation
        logout
        continue_process = true
      rescue Selenium::WebDriver::Error::UnhandledAlertError => e
        catch_error(e, "alert")
      rescue => e
        logout
        catch_error(e, "message")
      ensure
        kill_hanging_process
      end
      if continue_process
        close_browser
        process_content (sources)
      end
    end
    def login
      @browser = prepare_browser 'https://ibank.klikbca.com'
      log "Opening BCA Internet Banking"

      @browser.text_field(:name => 'value(user_id)').set @account_username
      @browser.text_field(:name => 'value(pswd)').set @account_password
      log "Loggin In"
      # @browser.select_list(:id => 'entry_1').selected? 'Ruby'
      @browser.button(:name => 'value(Submit)').click
    end
    def navigate_mutation
      @browser.frame(:name => "menu").wait_until_present
      @browser.frame(:name => "menu").link(:text=>/mutasi\srekening/i).wait_until_present
      log "find and click menu \"Mutasi Rekening\""
      @browser.frame(:name => "menu").link(:text=>/mutasi\srekening/i).click
    end
    def query_saldo
      @browser.frame(:name => "menu").wait_until_present
      @browser.frame(:name => "menu").link(:href=>/account_information_menu.htm/i).click
      @browser.frame(:name => "menu").link(:text=>/informasi\ssaldo/i).wait_until_present
      log "Query Balance"
      @browser.frame(:name => "menu").link(:text=>/informasi\ssaldo/i).click
      @browser.frame(:name => "atm").wait_until_present
      content = Nokogiri::HTML.parse(@browser.frame(:name => "atm").html)
      saldo = content.css("td:contains(\"#{@account_id}\") + td + td + td").text.gsub(/\s|\t|\n/, '').gsub(",","").to_f
      @bank.balances.create(amount: saldo, currency: 'IDR')
      log "Current balance : #{saldo}"
    end
    def query_mutation
      @browser.frame(:name => "atm").select_list(:name => "value(D1)").select(Regexp.new("#{@account_id.to_s}"))
      log "Selecting account id"
      @browser.frame(:name => "atm").select_list(:name => "value(startDt)").option(:value=>@start_date["date"]).select
      @browser.frame(:name => "atm").select_list(:name => "value(startMt)").option(:value=>@start_date["month"]).select
      @browser.frame(:name => "atm").select_list(:name => "value(startYr)").option(:value=>@start_date["year"]).select
      @browser.frame(:name => "atm").select_list(:name => "value(endDt)").option(:value=>@end_date["date"]).select
      @browser.frame(:name => "atm").select_list(:name => "value(endMt)").option(:value=>@end_date["month"]).select
      @browser.frame(:name => "atm").select_list(:name => "value(endYr)").option(:value=>@end_date["year"]).select
      log "Find data from #{@get_start_date.strftime('%Y-%m-%d')} to #{@get_end_date.strftime('%Y-%m-%d')}"
      @browser.frame(:name => "atm").button(:name => "value(submit1)").click

      @browser.frame(:name => "atm").wait_until_present
      log "Caputring content for processing"
      sources = @browser.frame(:name => "atm").html if @browser.frame(:name => "atm").exists?
    end
    def logout
      log "Logging Out"
      @browser.frame(:name => "header").link(:text=>/logout/i).click
    end
    def close_browser
      log "Closing Browser"
      @browser.close if @browser.exists?
    end
    def process_content (sources)
      mutations = []
      doc = Nokogiri::HTML(sources)
      contents = doc.css('body table:eq(3) tr:eq(2) tr:gt(1)')
      contents.each do |element|
        details = element.css('td')
        amount = details[3].text
        saldo = (details[5].text) || '0'
        is_credit = details[4].css('font').text.gsub("\n",'')=='CR'
        amount = amount.gsub("\n","")
        .gsub("\t","")
        .gsub(" ","")
        .gsub(',', '')
        amount = amount.to_f

        saldo = saldo.gsub("\n","")
        .gsub("\t","")
        .gsub(" ","")
        .gsub(',', '')

        description = details[1].css('font')[0].inner_html

        arr = description.split("<br>")
        from = arr.slice(-1,1)
        date = details[0].css('font')[0].text.to_s.gsub("\n","")
        date = date.gsub(/^(\d{2})\/(\d{2})$/, '\2/\1')
        if date.split("/").length == 2
          date = date+"/"+@start_date["year"]
        elsif /pend/i.match(date.downcase) != nil
          date = Time.now.utc.to_s
        end
        results = {
          "date"=> Chronic.parse(date).strftime('%Y-%m-%d'),
          "notes"=>description,
          "amount" => (is_credit&&amount)||(-amount)
        }

        results["old_key"] = create_key(
          "#{results["amount"]}#{saldo}#{results["date"]}"
        )
        results["serial_key"] = create_key(
          "#{results["amount"]}#{saldo}"
        )
        mutations << results
      end
      process_data(mutations, @bank, @get_start_date.strftime("%Y-%m-%d"))
      @job_id.update(status: 'completed')
    end
  end
end

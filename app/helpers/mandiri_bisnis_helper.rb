require 'RMagick'
include Magick
require_relative 'bank_helper'
module MandiriBisnisHelper
end
module BankHelper
  class MandiriBisnis < BankHelper::Bank
    def initialize (args)
      @engine = 'engine.cron.mandiri-bisnis'
      @start_date = Chronic.parse args[:start_date]
      @end_date = Chronic.parse args[:end_date]
      @account_username = args[:bank][:username]
      @account_corp_id = args[:bank][:corp_id]
      @account_password = args[:bank][:password]
      @account_id = args[:bank][:account_id]
      @bank = args[:bank]
      @engine = "engine.banks.mandiri_bisnis.#{@account_id}"
      @job_id = build_job(@engine)
      next_process  = false
      if caught_signal(@bank)
        log "Bank is still inactive"
        return
      end
      begin

        open_banking
        login
        query_saldo
        navigate_mutation
        contents = query_mutation
        logout
        next_process = true

      rescue Selenium::WebDriver::Error::UnhandledAlertError => e
        catch_error(e, 'alert')
      rescue => e
        logout
        catch_error(e, 'message')
      ensure
        kill_hanging_process
      end
      if next_process
        log "closing browser and end of session"
        close_browser
        process_content(contents)
      end
    end
    def open_banking
      @browser = prepare_browser 'https://mib.bankmandiri.co.id/sme/common/login.do?action=loginPopupRequestSME'
      log "Opening Mandiri Bisnis Internet Banking"
      sleep(2)
      @browser.goto 'https://mib.bankmandiri.co.id/sme/common/login.do?action=loginRequestSME2'
    end
    def login

      @browser.text_field(:name => 'corpId').set @account_corp_id
      @browser.text_field(:name => 'userName').set @account_username
      @browser.text_field(:name => 'passwordEncryption').set @account_password
      log "Logging In"
      @browser.button(:type => 'submit').click
    end
    def query_saldo
      @browser.frame(:name => "mainFrame").wait_until_present
      content = Nokogiri::HTML.parse(@browser.frame(:name => "mainFrame").html)
      saldo = content.css("td:contains(\"#{@account_id}\") + td").text.gsub('IDR', '').gsub(/\s|\t|\n/, '').gsub(",", "").to_f
      @bank.balances.create(amount: saldo, currency: 'IDR')
      log "Current balance : #{saldo}"
    end
    def navigate_mutation
      @browser.frame(:name => "menuFrame").wait_until_present
      @browser.frame(:name => "menuFrame").link(:text=>"Informasi Rekening").click
      @browser.frame(:name => "menuFrame").link(:text=>"Mutasi Rekening").wait_until_present
      log "Looking for mutation menu"
      @browser.frame(:name => "menuFrame").link(:text=>"Mutasi Rekening").click
    end
    def query_mutation
      @browser.frame(:name => "mainFrame").select_list(:name=>"dlAccountNo").select(Regexp.new("#{@account_id.to_s}"))

      @browser.frame(:name => "mainFrame").hidden(:name=>"transferDateDay1").set @start_date.day.to_s
      @browser.frame(:name => "mainFrame").hidden(:name=>"transferDateMonth1").set @start_date.month.to_s
      @browser.frame(:name => "mainFrame").hidden(:name=>"transferDateYear1").set @start_date.year.to_s

      @browser.frame(:name => "mainFrame").hidden(:name=>"transferDateDay2").set @end_date.day.to_s
      @browser.frame(:name => "mainFrame").hidden(:name=>"transferDateMonth2").set @end_date.month.to_s
      @browser.frame(:name => "mainFrame").hidden(:name=>"transferDateYear2").set @end_date.year.to_s
      log "Query data from #{@start_date.strftime('%Y-%m-%d')} to #{@end_date.strftime('%Y-%m-%d')}"

      @browser.frame(:name => "mainFrame").button(:name=>"show1").click
      @browser.frame(:name => "mainFrame").table(:class => "clsFormTrxStatus").wait_until_present
      contents = []
      content = @browser.frame(:name => "mainFrame").html
      total_page = Nokogiri::HTML.parse(content).css('.clsBtnBar td').text.gsub("\n","").gsub(/\s+/,"").match(/(\d+)/i)[1].to_i

      log "Total pages found : #{total_page}"
      contents << content

      if total_page > 1
        i = 1
        while i < total_page do
          i += 1
          @browser.frame(:name => "mainFrame").table(:class => "clsFormTrxStatus").text_field(:name => "valuePage").set i
          @browser.frame(:name => "mainFrame").table(:class => "clsFormTrxStatus").button(:name => "Go").click
          log "Go to page #{i}"
          @browser.frame(:name => "mainFrame").table(:class => "clsFormTrxStatus").wait_until_present
          contents << @browser.frame(:name => "mainFrame").html
        end
      end
      contents
    end
    def logout
      log "logging out"
      @browser.frame(:name => "topFrame").link(:href=>/\?action\=logoutSME/).click if @browser.frame(:name => "topFrame").link(:href=>/\?action\=logoutSME/).exists?
    end
    def process_content(contents)
      log "parsing data"
      rows = []
      contents.each do |content|
        content = Nokogiri::HTML.parse(content)
        content.css('table.clsFormTrxStatus tr.clsEven').each do |el|
          if el.css('td').length == 6
            date_time = el.at_css('td:eq(1)').text
            amount =  -(el.at_css('td:eq(4)').text.gsub(",", "").to_f) if el.at_css('td:eq(4)').text.to_f != 0
            amount = el.at_css('td:eq(5)').text.gsub(",", "").to_f unless el.at_css('td:eq(4)').text.to_f != 0
            mutation = {
              "date" => Date.parse(el.at_css('td:eq(1)').text).strftime('%Y-%m-%d'),
              "notes" => el.at_css('td:eq(3)').text.gsub("\r", "").gsub("\t", "").gsub("\n", "").gsub(/^\s+|\s$/, "").split.join(" "),
              "amount" => amount,
              "date_time"=>date_time
            }
            saldo = el.at_css('td:eq(6)').text.gsub("\r", "").gsub("\t", "").gsub("\n", "").gsub(/^\s+|\s$/, "")
            mutation["old_key"] = create_key("#{mutation["date"]}#{saldo}#{mutation["amount"]}")
            mutation["serial_key"] = create_key("#{date_time}#{mutation["amount"]}")
            rows << mutation
          end
        end
      end

      process_data(rows, @bank, @start_date.strftime("%Y-%m-%d"))
      # log "sleep for a while"

      @job_id.update(status: 'completed')
    end
  end
end

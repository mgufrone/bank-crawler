module JobHelper
  def self.register_worker(name, worker, argument, notation)
    queue_worker(name, worker, argument, notation)
  end
  def queue_worker(name, worker, argument, notation)

    if Sidekiq::Queue.new.select {|e| e.item["args"] == argument && e.item["class"] == worker} != []
      response = {:message => "Job already in queue"}
    elsif Sidekiq::RetrySet.new.select {|e| e.item["args"] == argument && e.item["class"] == worker} != []
      response = {:message => "Job already in retry job list"}
    elsif Sidekiq::ScheduledSet.new.select {|e| e.item["args"] == argument && e.item["class"] == worker} != []
      response = {:message => "Job already scheduled"}
    else
      if notation == 'once'
        if Sidekiq::Cron::Job.find("bank-#{name.gsub(' ', '-').downcase}") == nil
          register_cron ({name: "bank-#{name.gsub(' ', '-').downcase}", args: argument, cron: '0,30 * * * *', klass: worker})
          Sidekiq::Cron::Job.find("bank-#{name.gsub(' ', '-').downcase}").enque!
        else
          Sidekiq::Cron::Job.find("bank-#{name.gsub(' ', '-').downcase}").enque!
        end
        response = {:message => "Job queued"}
      else
        if (job=Sidekiq::Cron::Job.find("bank-#{name.gsub(' ', '-').downcase}")) == nil

          job = register_cron ({name: "bank-#{name.gsub(' ', '-').downcase}", args: argument, cron: notation, klass: worker})
          response = {:message => "Job registered"}
        else
          if job.cron == notation
            response = {:message => "Job already scheduled"}
          else
            job.destroy
            job = register_cron ({name: "bank-#{name.gsub(' ', '-').downcase}", args: argument, cron: notation, klass: worker})
            response = {:message => "Job replaced"}
          end
        end
      end
    end
  end
  def register_cron(args)

    job = Sidekiq::Cron::Job.new(name: "#{args[:name]}", args: args[:args], cron: args[:cron], klass: args[:klass])
    if job.valid?
      job.save
    else
      puts job.errors
    end
    job
  end
  def destroy(name)
    Sidekiq::Cron::Job.find("bank-#{name}").destroy if Sidekiq::Cron::Job.find("bank-#{name}") != nil
  end
end

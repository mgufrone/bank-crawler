require_relative 'logger_helper'
require_relative 'notification_helper'
require 'chronic'
# module Watir
#   class Hidden < Input
#     # Set the value of a hidden input field.
#     def set(value)
#       self.browser.execute_script('arguments[0].setAttribute("value", arguments[1]);', self, value)
#     end
#   end
# end
module BankHelper
  class Bank
    include LoggerHelper
    include NotificationHelper

    def initialize

    end
    def log (message, show_output=true, result='')
      @job_id.log_messages.create(message: message, name: @engine, result: (show_output && take_screenshot(message) || result),status:'')
      puts "[#{@engine}] #{message}" if show_output
    end
    def create_key (content)
      Digest::MD5.hexdigest(
        content
      ).to_s.scan(/.{4}/).join("-")
    end
    def prepare_browser(url, browser=:chrome)
      @current_browser = browser
      browser = Watir::Browser.start url, :phantomjs
      screen_width = browser.execute_script("return screen.width;")
      screen_height = browser.execute_script("return screen.height;")
      browser.driver.manage.window.resize_to(screen_width,screen_height)
      browser.driver.manage.window.move_to(0,0)
      browser
    end
    def take_screenshot (message)
      if @browser != nil
        file_name = create_key("#{message}-#{Time.now.to_s}")
        # file_name = "#{message.downcase.tr('^A-Za-z0-9 ','').gsub(' ', "-")}"
        if File.exists?(Rails.root.join("log/screenshots")) == false
          Dir.mkdir Rails.root.join("log/screenshots")
        end
        if File.exists?(Rails.root.join("log/screenshots/#{@job_id.job_id}")) == false
          Dir.mkdir Rails.root.join("log/screenshots/#{@job_id.job_id}")
        end
        if @browser.exists?
          @browser.screenshot.save(Rails.root.join("log/screenshots/#{@job_id.job_id}/#{file_name}.png"))
          ["http://crawlers.terapkan.com",
            Rails.application.routes.url_helpers.screenshot_path(format: :png, file: file_name, dir: @job_id.job_id)
          ].join('')
        else
          ''
        end
      else
        ''
      end
    end
    def caught_signal (bank)
      bank.status == 'inactive'
    end
    def process_data (mutations, bank, start_date)

      # ActiveRecord::Base.connection.execute("ALTER TABLE `transactions` auto_increment = #{Transaction.count+1}; ");
      log "Inserting data"
      mutation_logs = {
        :new => 0,
        :update => 0,
        :duplicates => 0,
        :old => 0
      }
      mutations.uniq.each do |results|
        if results.include?('old_key') and Transaction.where("`serial_key` = ?", results["old_key"]).count == 1
          mutation_logs[:old]+=1
          trans = Transaction.find_by(serial_key: results["old_key"])
          trans.serial_key = results["serial_key"]
          trans.date = results["date"]
          trans.save
        elsif Transaction.where("`serial_key` = ?", results["serial_key"]).count > 0
          mutation_logs[:update]+=1
          trans = Transaction.find_by(serial_key: results["serial_key"])
          trans.date = results["date"]
          trans.notes = results["notes"]
          trans.save
        elsif Transaction.where("`serial_key` = ?", results["serial_key"]).count == 0
          if @bank.transactions.where("date between ? and ? and amount = ?", Chronic.parse("7 days ago", :now => Chronic.parse(results["date"])).strftime('%Y-%m-%d'), results["date"], results["amount"]).count == 1 and ['mandiri', 'mandiri-bisnis', 'mandiri_bisnis'].include?(@bank.bank_type)
            @bank.duplicates.find_or_create_by(serial_key: results["serial_key"]){|trans|
              trans.date = results["date"]
              trans.amount = results["amount"]
              trans.notes = results["notes"]
              trans.content = @bank.transactions.where("date between ? and ? and amount = ?", Chronic.parse("7 days ago", :now => Chronic.parse(results["date"])).strftime('%Y-%m-%d'), results["date"], results["amount"]).first.id
              mutation_logs[:duplicates]+=1
            }

          else
            mutation_logs[:new]+=1
            new_mutation = @bank.transactions.create(notes: results["notes"], date: results["date"], amount: results["amount"], serial_key: results["serial_key"])
            results["id"] = new_mutation.id
          end
        end
      end
      if mutation_logs[:new] >= 0
        notify "New transactions in #{@bank.name}", "Total #{mutation_logs[:new]} new transactions found in #{@bank.name}. Please take a look at it", @bank.user_id
      end
      log "Summary of mutations : #{mutation_logs}"
      log "check for duplicate records"

      mutations.group_by {|e| e}.each do |value|
        results = value[0]
        if value[1].count > 1 and DuplicateTransaction.where("`serial_key` = ?", results["serial_key"]).count == 0
          DuplicateTransaction.create(notes: results["notes"], date: results["date"], amount: results["amount"], bank_id: bank.id, content: value[1].to_json, serial_key: results["serial_key"])
          log "Making note for duplicate records. Serial Key : #{results["serial_key"]}"
        end
      end
      log("saving raw results", false, mutations.to_json)
      kill_hanging_process
      log "done"
      trigger_callback(@bank.name, "Bank Crawler completed", "success")
    end
    def kill_hanging_process
      current_browser = "chrome" if @current_browser == :chrome
      current_browser = "firefox" unless @current_browser == :chrome
      output = `ps -ef | grep #{current_browser}`.split("\n")
      pids = []
      output.each do |line|
        data = line.split(" ")
        date = Chronic.parse("today #{data[4]}")
        # puts Time.now - date
        `kill -9 #{data[1]}` if Time.now - date > 10*60 # kill all firefox process more than 10 minutes range. since it's useless
      end
      `rm core.*`
      log "clean up some unnecessary files"
    end
    def catch_error(e, type)
      if @browser != nil
        message = @browser.alert.text if @browser.alert.exists? and type == 'alert'
        message = e.message unless @browser.alert.exists? and type == 'alert'
      else
        message = e.message
      end
      message = "Error Occured (#{message})"
      log message
      close_browser
      backtrace_string = ["Trace : "]
      e.backtrace.each {|line| backtrace_string << line}
      @job_id.update(status: 'error')
      log(backtrace_string.join("\n"), false)
      trigger_callback(@bank.name, message, "error")
      raise e.message
    end
    def trigger_callback(bank_name, message, status)
      return if File.exists?("#{Rails.root.join("tmp")}/callbacks.json") == false
      callbacks = JSON.parse(File.read("#{Rails.root.join("tmp")}/callbacks.json"))
      if callbacks.include?("bank-#{bank_name.gsub(' ', '-').downcase}") and callbacks["bank-#{bank_name.gsub(' ', '-').downcase}"].size > 0
        log "Triggering registered callbacks"
        callbacks["bank-#{bank_name.gsub(' ', '-').downcase}"].each do |callback|
          callback["arguments"][0]["data"]["result"] = {
            "status"=>status,
            "message"=>message,
          }
          Kernel.const_get(callback["worker"]).perform_async(callback["arguments"][0])
        end
        callbacks["bank-#{bank_name.gsub(' ', '-').downcase}"] = []
        File.open("#{Rails.root.join("tmp")}/callbacks.json", "w"){|file| file.write(callbacks.to_json)}
        log "Cleanup callbacks"
      end
    end
    def close_browser
      @browser.close if @browser != nil and @browser.exists?
    end
  end
end

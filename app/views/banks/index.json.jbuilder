json.array!(@banks) do |bank|
  json.extract! bank, :id, :name, :username, :password, :account_id, :company_id, :bank_type, :status
  json.url bank_url(bank, format: :json)
end

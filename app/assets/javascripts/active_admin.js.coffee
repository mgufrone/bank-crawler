#= require active_admin/base
#= require jquery

#= require pace/pace
#= require turbolinks
#= require fancybox
#= require angular
#= require angular-animate
#= require angular-resource
#= require banks
#= require angular-audio
#= require notification

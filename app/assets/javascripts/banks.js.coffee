# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
ready = ->
  $(".ajax-link").click (e) ->
    e.preventDefault()
    $.post $(this).attr("href"), ((data) ->
      alert data.message
    ), 'json'

$(document).ready(ready)
$(document).on('page:load', ready)

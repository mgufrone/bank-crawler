# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
angular.module('easy-bank', ['ngAudio']).controller 'NotificationController', [
  '$http',
  '$scope',
  '$log',
  'ngAudio',
  ($http, $scope, $log, ngAudio) ->
    notif = this
    $log.info "Already run"
    notif.setup = (data) ->
      angular.forEach data, (value, key)->
        notif[key] = value

    if !"Notification" in window
      alert "Browser doesn't support desktop notification"
    else
      notif.query = (immediately) ->
        immediately = if typeof immediately == 'undefined' then false else immediately
        querying = ->
          $http.get("/notification/query").success((data)->
            angular.forEach data, (value, key) ->
              if value.splashed == false || value.splashed == null
                notif.splash value
              return
            notif.query()
          ).error((data)->
            notif.query()
          )
        if immediately
          querying()
        else
          setTimeout (e) ->
            querying()
          , 5000
        return
      notif.splash = (notification) ->
        if Notification.permission == "granted"
          notification = new Notification notification.title, body: notification.message, tag: notification.id
          notification.onshow = (n) ->
            notif.splashed(n)
        else if Notification.permission != "denied"
          Notification.requestPermission (permission) ->
            if permission == "granted"
              notification = new Notification notification.title, body: notification.message, tag: notification.id
              notification.onshow = (n) ->
                notif.splashed(n)
        return
      notif.splashed = (id) ->
        # $log.info(id.target.tag)
        ngAudio.load("/sounds/cha-ching.mp3").play()
        $http.get("/notification/:notification/splashed".replace ":notification", id.target.tag).success((data)->
          $log.info data
        ).error((e) ->
          setTimeout (e)->
            notif.splashed id
          , 3000
        )
      notif.query true
    return
]

$(window).load(->
  jQuery('body').attr 'ng-controller', 'NotificationController'
  angular.bootstrap document, ['easy-bank']
)

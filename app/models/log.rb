class Log < ActiveRecord::Base
	has_many :log_messages, class_name: "LogMessage", foreign_key: "log_id"
end

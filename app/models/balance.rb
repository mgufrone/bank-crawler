class Balance < ActiveRecord::Base
	self.table_name = "bank_balances"
	belongs_to :bank, class_name: "Bank", foreign_key: "bank_id"
end

class LogMessage < ActiveRecord::Base
	belongs_to :logs, class_name: "Log", foreign_key: "log_id"
end

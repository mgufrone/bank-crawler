class Bank < ActiveRecord::Base
  validates :account_id, presence: true
  validates :username, presence: true
  validates :name, presence: true
  validates :password, presence: true, on: :create
  validates :bank_type, presence: true
  validates :notation, presence: true
  validates :account_id, uniqueness: {scope: [:bank_type, :company_id], case_sensitive: false}, on: :create

  enum bank_types: {"Bank BCA"=>1, "Bank Mandiri"=>2, "Bank Mandiri Bisnis"=>3, "Bank BNI"=>4}
  enum bank_codes: {"bca"=>1, "mandiri"=>2, "mandiri_bisnis"=>3, "bni"=>4}
  enum statuses: [:inactive, :active]
  enum notations: {"Every 15 minutes" => "*/15 * * * *", "Every 30 minutes" => "0,30 * * * *", "Every hour"=>"0 */1 * * *","Every 2 Hour"=>"0 */2 * * *"}

  has_many :transactions
  has_many :duplicates, class_name: "DuplicateTransaction", foreign_key: "bank_id"
  has_many :balances, class_name: "Balance", foreign_key: "bank_id"
  belongs_to :user, class_name: "AdminUser", foreign_key: "user_id"
  scope :by_id, -> (id) {where("user_id = ?", id)}
end

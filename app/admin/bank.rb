include JobHelper
ActiveAdmin.register Bank do
  # scope_to do
  #   # Bank.by_id(current_admin_user.id)
  # end
  permit_params :name, :bank_type, :account_id, :username, :password, :status, :id, :notation, :company_id
  batch_action :queue_now, confirm: "Are you sure?" do |banks|
    bank_name = []
    Bank.find(banks).each do |bank|
      @bank = bank
      JobHelper.register_worker("#{Bank.bank_codes.invert[@bank.bank_type]}-#{@bank.id}", "BankWorker", [@bank.id], "once")
      bank_name << bank.name
    end
    redirect_to collection_path, notice: "#{bank_name.join(", ")} has been queued"
  end
  member_action :queue, method: :post do
    @bank = resource
    response = JobHelper.register_worker("#{Bank.bank_codes.invert[@bank.bank_type]}-#{@bank.id}", "BankWorker", [@bank.id], "once")
    render :json => response
  end
  after_create do |bank|
    @bank = bank
    bank.user = current_admin_user
    bank.save
    JobHelper.register_worker("#{Bank.bank_codes.invert[@bank.bank_type]}-#{@bank.id}", "BankWorker", @bank.id, @bank.notation)
  end
  after_update do |bank|
    @bank = bank
    bank.user = current_admin_user
    bank.save
    JobHelper.register_worker("#{Bank.bank_codes.invert[@bank.bank_type]}-#{@bank.id}", "BankWorker", @bank.id, @bank.notation)
  end
  before_destroy do |bank|
    @bank = bank
    JobHelper.destroy("#{Bank.bank_codes.invert[@bank.bank_type]}-#{@bank.id}")
  end
  controller do
    def scoped_collection
      Bank.by_id(current_admin_user.id).joins("LEFT JOIN bank_balances ON banks.id = bank_balances.bank_id").group(:bank_id)
      .select('banks.*', 'bank_balances.amount as balance_amount')
    end
    def destroy
      destroy!
    end
  end
  index do
    selectable_column
    # id_column
    column :name
    column :bank_type do |bank|
      # bank.bank_type
      Bank.bank_types.invert[bank.bank_type]
    end
    column :account_id
    column :total_transactions do |bank|
      link_to "#{bank.transactions.count} #{'Transaction(s)'}", admin_transactions_url(q: {:bank_id_eq=> bank.id}, :commit => "Filter")
    end
    column :last_successful_run do |bank|
      data = Log.where('name = ?', "engine.banks.#{Bank.bank_codes.invert[bank.bank_type]}.#{bank.account_id}").order(created_at: :desc).first
      if data == nil
        "Not queried yet"
      else
        if data.status == 'error'
          "#{status_tag 'error'} #{data.log_messages.order(id: :desc)[1].message.gsub('Error Occured ( ', '').gsub(/\)$/, '')}"
        elsif data.status == 'running'
          status_tag "running", :ok, class: "label label-warning", label: "Currently Running"
        else
          status_tag "done", :ok, label: "#{data.created_at.strftime "%d/%m/%y %H:%M"}"
        end
      end
    end
    column "Current balance" do |bank|
      # bank.balances.last.amount.to_f
      # if bank.balances.count > 0
        number_to_currency(bank.balance_amount.to_f, unit: "Rp. ", separator: ",", delimiter: ".")
      # end
    end
    # column :username
    # column :password
    # actions
    actions do |bank|
      link_to "Query Now", queue_admin_bank_url(id: bank.to_param), class: "ajax-link"
    end
  end
  show do
    panel "Details" do
      attributes_table_for bank do

        data = Log.where('name = ?', "engine.banks.#{Bank.bank_codes.invert[bank.bank_type]}.#{bank.account_id}").order(created_at: :desc).first

        row :name
        row :username
        row :password
        row "Last Run Time" do
          if data != nil
            data.created_at.strftime "%d/%m/%y %H:%M"
          else
            "Not queried yet"
          end
        end
        row "Last Run Message" do
          if data != nil
            status_tag data.status
          else
            "Not queried yet"
          end

        end
        row :last_message do
          if data != nil
            if data.status == 'error'
              data.log_messages.order(id: :desc)[1].message
            else
              data.log_messages.last.message unless data.log_messages.last.nil?
            end
          else
            "Not queried yet"
          end
        end
        row "Run every" do
          Bank.notations.invert[bank.notation]
        end
      end
    end
  end
  filter :name
  # filter :bank_type
  # filter :account_id
  filter :username
  # filter :password

  form do |f|
    f.inputs "Bank Data" do
      f.input :name
      f.input :bank_type, :as => :select, :collection => Bank.bank_types
      f.input :account_id
      f.input :company_id
      f.input :username
      f.input :password
      f.input :status, :as => :radio  , :label => "Activate?", :collection => ["Inactive", "Active"]
      f.input :notation, :as => :select, :collection => Bank.notations, label: "Query this bank"
    end
    f.actions
  end

end

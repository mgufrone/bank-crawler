ActiveAdmin.register LogMessage do
  # belongs_to :log
  # menu priority: 120
  menu false
  config.clear_action_items!
  index as: :block do |message|
    # !resource_selection_cell message
    div for: message do

      host = request.host if Rails.env == :production
      host = "#{request.host}:#{request.port}" unless Rails.env == :production
      image_tag(message.result.gsub("crawlers.terapkan.com", host), width: '100%') if message.result.nil? == false and message.result =~ URI::regexp
    end
    h2 raw(message.message)
    # h2  auto_link     message.name
    # div raw(message.message)
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end

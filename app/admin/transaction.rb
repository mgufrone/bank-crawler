ActiveAdmin.register Transaction do
  # permit_params :bank_id, :bank_type, :account_id, :username, :password
  menu false
  config.sort_order = "date_desc"
  config.per_page = 100
  config.clear_action_items!
  index do
    selectable_column
    id_column
    column :date
    column :notes do |transaction|
      raw transaction.notes
    end
    column "Kredit" do |transaction|
      # status = :none
      amount = transaction.amount
      if amount < 0
        amount = 0
      end
      number_to_currency(amount, unit: "Rp. ", separator: ",", delimiter: ".")
    end
    column "Debit" do |transaction|
      status = :none
      amount = transaction.amount
      if amount > 0
        amount = 0
        # amount = 0
      else
        amount = -(amount)
      end
      number_to_currency(amount, unit: "Rp. ", separator: ",", delimiter: ".")
    end
    # column :bank_type do |bank|
    #   # bank.bank_type
    #   Bank.bank_types.invert[bank.bank_type]
    # end
    # column :account_id
    column "Bank", sortable: :bank_id do |transaction|
      transaction.bank.name
    end
    # column :total_transactions do |bank|
    #   "#{bank.transactions.count} #{'Transaction'.pluralize}"
    # end

    # column :username
    # column :password
    # actions defaults: false do |bank|
    #   # action_item :view
    #   # link_to "Query Now", "#", class: "ajax-link"
    # end
  end

  filter :notes
  filter :date
  filter :bank
  show do
    panel "Details" do
      attributes_table_for transaction do
        row :bank
        row :notes do
          raw(transaction.notes)
        end
        row :amount do
          number_to_currency(transaction.amount, unit: "Rp. ", separator: ",", delimiter: ".")
        end
      end
      # render partial: "details", locals: {transaction: transaction}
    end
  end
  # form do |f|
  #   f.inputs "Transactions" do
  #     f.input :name
  #     f.input :bank_type, :as => :select, :collection => Bank.bank_types
  #     f.input :account_id
  #     f.input :username
  #     f.input :password
  #     f.input :status, :as => :radio  , :label => "Activate?", :collection => Bank.statuses
  #   end
  #   f.actions
  # end

end

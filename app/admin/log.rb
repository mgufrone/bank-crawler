ActiveAdmin.register Log do

  menu :priority => 100
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  controller do
    def scoped_collection
      Log.joins("INNER JOIN banks ON logs.name LIKE CONCAT('%', banks.account_id)")
      .select('logs.*', 'banks.name').where("banks.user_id = ?", current_admin_user.id)
      # super.includes :log_messages
    end
  end

  config.sort_order = "created_at_desc"
  config.clear_action_items!
  index do
    column :name do |log|
      link_to "#{log.name} - #{log.id}", admin_log_messages_url(q: {:log_id_eq=> log.id}, :commit => "Filter")
    end
    column :status do |log|
      status_tag log.status
    end
    column :last_message do |log|
      if log.log_messages.last.nil? == false
        if log.status != 'error'
          log.log_messages.last.message
        else
          log.log_messages.order(id: :desc)[1].message
        end
      else
        ""
        # log.log_messages.first.to_json
      end
    end
    column "Started At", :created_at
    column "End At", :updated_at
    column "Duration",sortable: "updated_at-created_at" do |log|
      "#{(log.updated_at - log.created_at).to_i} seconds"
    end
  end
end

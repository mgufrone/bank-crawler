# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150209144456) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "",    null: false
    t.string   "encrypted_password",     limit: 255, default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_admin",               limit: 1,   default: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "bank_balances", force: :cascade do |t|
    t.integer  "bank_id",    limit: 4
    t.decimal  "amount",                 precision: 20, scale: 2
    t.string   "currency",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bank_balances", ["bank_id"], name: "fk_rails_ac89e306bb", using: :btree

  create_table "banks", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "username",   limit: 255
    t.string   "password",   limit: 255
    t.string   "account_id", limit: 255
    t.string   "company_id", limit: 255
    t.integer  "bank_type",  limit: 4
    t.string   "status",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "notation",   limit: 255
    t.integer  "user_id",    limit: 4
  end

  add_index "banks", ["user_id"], name: "fk_rails_526ccaf11f", using: :btree

  create_table "duplicate_transactions", force: :cascade do |t|
    t.integer  "bank_id",    limit: 4
    t.text     "notes",      limit: 65535
    t.decimal  "amount",                   precision: 20, scale: 2
    t.string   "serial_key", limit: 255
    t.date     "date"
    t.string   "currency",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "duplicate_transactions", ["bank_id"], name: "fk_rails_de4fe2d985", using: :btree

  create_table "log_messages", force: :cascade do |t|
    t.integer  "log_id",      limit: 4
    t.text     "message",     limit: 65535
    t.string   "message_id",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",        limit: 255
    t.text     "description", limit: 65535
    t.text     "result",      limit: 65535
    t.string   "status",      limit: 255
  end

  add_index "log_messages", ["log_id"], name: "fk_rails_e04ae7ab84", using: :btree

  create_table "logs", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "action",     limit: 255
    t.string   "job_id",     limit: 255
    t.string   "status",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "status",     limit: 255,   default: "unread"
    t.text     "message",    limit: 65535
    t.string   "notif_type", limit: 255,   default: "info"
    t.boolean  "splashed",   limit: 1
    t.boolean  "mail_sent",  limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title",      limit: 255
  end

  add_index "notifications", ["user_id"], name: "fk_rails_73a0cef6a6", using: :btree

  create_table "transactions", force: :cascade do |t|
    t.integer  "bank_id",    limit: 4
    t.text     "notes",      limit: 65535
    t.decimal  "amount",                   precision: 20, scale: 2
    t.string   "serial_key", limit: 255
    t.date     "date"
    t.string   "currency",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "transactions", ["bank_id"], name: "fk_rails_2ef8237ff3", using: :btree

  add_foreign_key "bank_balances", "banks"
  add_foreign_key "banks", "admin_users", column: "user_id"
  add_foreign_key "duplicate_transactions", "banks"
  add_foreign_key "log_messages", "logs"
  add_foreign_key "notifications", "admin_users", column: "user_id"
  add_foreign_key "transactions", "banks"
end

class CreateBankBalances < ActiveRecord::Migration
  def change
    create_table :bank_balances do |t|
      t.integer :bank_id
      t.decimal :amount, precision: 20, scale: 2
      t.string :currency
      t.timestamps
    end
    add_foreign_key :bank_balances, :banks
  end
end

class AddNotationOnBank < ActiveRecord::Migration
  def change
    add_column :banks, :notation, :string
  end
end

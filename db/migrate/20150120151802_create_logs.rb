class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :name
      t.string :action
      t.string :job_id
      t.string :status
      t.timestamps
    end
  end
end

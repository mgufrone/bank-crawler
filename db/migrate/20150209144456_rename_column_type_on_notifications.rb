class RenameColumnTypeOnNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :title, :string
    rename_column :notifications, :type, :notif_type
    change_column :notifications, :message, :text
  end
end

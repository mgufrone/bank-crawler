class CreateLogMessages < ActiveRecord::Migration
  def change
    create_table :log_messages do |t|
      t.integer :log_id
      t.text :message
      t.string :message_id
      t.timestamps
    end
    add_foreign_key :log_messages, :logs
  end
end

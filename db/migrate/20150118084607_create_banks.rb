class CreateBanks < ActiveRecord::Migration
  def change
    create_table :banks do |t|
      t.string :name
      t.string :username
      t.string :password
      t.string :account_id
      t.string :company_id
      t.integer :bank_type
      t.string :status

      t.timestamps
    end
  end
end

class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.integer :bank_id
      # add
      t.text :notes
      t.decimal :amount, precision: 20, scale: 2
      t.string :serial_key
      t.date :date
      t.string :currency
      t.timestamps
    end
    add_foreign_key :transactions, :banks
  end
end

class AddUserIdOnBanks < ActiveRecord::Migration
  def change
    add_column :banks, :user_id, :int
    add_foreign_key :banks, :admin_users, column: :user_id

  end
end

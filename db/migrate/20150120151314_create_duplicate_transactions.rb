class CreateDuplicateTransactions < ActiveRecord::Migration
  def change
    create_table :duplicate_transactions do |t|
      t.integer :bank_id
      # add
      t.text :notes
      t.decimal :amount, precision: 20, scale: 2
      t.string :serial_key
      t.date :date
      t.string :currency
      t.timestamps
    end
    add_foreign_key :duplicate_transactions, :banks
  end
end

class AddDescriptionOnLogMessages < ActiveRecord::Migration
  def change
    add_column :log_messages, :name, :string
    add_column :log_messages, :description, :text
    add_column :log_messages, :result, :text
    add_column :log_messages, :status, :string
  end
end

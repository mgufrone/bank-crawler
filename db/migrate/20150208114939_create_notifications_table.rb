class CreateNotificationsTable < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.string :status, default: 'unread'
      t.string :message
      t.string :type, default: 'info'
      t.boolean :splashed
      t.boolean :mail_sent
      t.timestamps
    end
    add_foreign_key :notifications, :admin_users, column: :user_id
  end
end

require 'test_helper'

class ApiControllerTest < ActionController::TestCase
  test "should get jobs" do
    get :jobs
    assert_response :success
  end

  test "should get queued_jobs" do
    get :queued_jobs
    assert_response :success
  end

  test "should get register_job" do
    get :register_job
    assert_response :success
  end

end

FROM ruby:2.2
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev
RUN mkdir /myapp
WORKDIR /myapp
ARG WORKER=false
ENV WORKER ${WORKER}
RUN curl -sL https://deb.nodesource.com/setup_7.x | bash -
RUN apt-get install -y nodejs
RUN echo $WORKER
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install yarn -y
RUN if [ ${WORKER} = true ]; then \
  apt-get install -y xvfb zip \
  && apt-get install libmagickwand-dev imagemagick -y \
  && curl https://chromedriver.storage.googleapis.com/2.31/chromedriver_linux64.zip -o chromedriver.zip \
  && unzip chromedriver.zip \
  && cp chromedriver /usr/bin/chromedriver \
  && curl -L -o geckodriver.tar.gz https://github.com/mozilla/geckodriver/releases/download/v0.18.0/geckodriver-v0.18.0-linux64.tar.gz \
  && ls -la geckodriver.tar.gz \
  && tar -xzf geckodriver.tar.gz \
  && cp geckodriver /usr/bin \
  && curl -L -o firefox.tar.bz 'https://download.mozilla.org/?product=firefox-55.0.1-SSL&os=linux64&lang=id' \
  && tar -xjf firefox.tar.bz \
  && ln -s $(pwd)/firefox/firefox-bin /usr/local/bin/firefox \
;fi
RUN npm install -g bower
ADD Gemfile /myapp/Gemfile
ADD Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
ADD . /myapp
